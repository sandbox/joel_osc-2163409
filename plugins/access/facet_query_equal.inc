<?php

/**
 * @file
 * Plugin to provide access control/visibility based on specified context query
 * string matching user-specified string
 */

$plugin = array(
  'title' => t("Facet Query string: comparison"),
  'description' => t('Control access by facet query string match.'),
  'callback' => 'ctools_facet_query_equal_access_check',
  'settings form' => 'ctools_facet_query_equal_access_settings',
  'summary' => 'ctools_facet_query_equal_access_summary',
  'required context' => new ctools_context_required(t('Query string'), 'facet_query'),
  'defaults' => array('operator' => '=', 'value' => '', 'case' => FALSE),
);

/**
 * Settings form
 */
function ctools_facet_query_equal_access_settings($form, &$form_state, $conf) {
  $form['settings']['operator'] = array(
    '#type' => 'radios',
    '#title' => t('Operator'),
    '#options' => array(
      '=' => t('Equal'),
      '!=' => t('Not equal'),
    ),
    '#default_value' => $conf['operator'],
  );

  $form['settings']['value'] = array(
    '#type' => 'textfield',
    '#title' => t('String'),
    '#default_value' => $conf['value'],
  );

  $form['settings']['case'] = array(
    '#type' => 'checkbox',
    '#title' => t('Case sensitive'),
    '#default_value' => $conf['case'],
  );
  return $form;
}

/**
 * Check for access
 */
function ctools_facet_query_equal_access_check($conf, $context) {
  if (empty($context) || empty($context->data)) {
    $string = '';
  }
  else {
    $string = $context->data;
  }

  $value = $conf['value'];
  if (empty($conf['case'])) {
    $string = drupal_strtolower($string);
    $value = drupal_strtolower($value);
  }

  switch ($conf['operator']) {
    case '=':
      return $string === $value;
    case '!=':
      return $string !== $value;
  }
}

/**
 * Provide a summary description based upon the specified context
 */
function ctools_facet_query_equal_access_summary($conf, $context) {
  $values = array(
    '@identifier' => $context->identifier,
    '@value' => $conf['value']
  );
  switch ($conf['operator']) {
    case '=':
      return t('@identifier is "@value"', $values);
    case '!=':
      return t('@identifier is not "@value"', $values);
  }
}
