<?php

/**
 * @file facet_query.inc
 * Context plugin that can extract arbitrary values from the query string.
 */

/**
 * $plugin array which will be used by the system that includes this file.
 */
$plugin = array(
  'title' => t('Facet query string value'),
  'description' => t('A context that extracts a value from a facet query string.'),
  'context' => 'ctools_facet_query_context_create_facet_query',
  'context name' => 'facet_query',
  'keyword' => 'facet_query',
  'edit form' => 'ctools_facet_query_context_settings_form',
  'convert list' => 'ctools_facet_query_context_convert_list',
  'convert' => 'ctools_facet_query_context_convert',
);

/**
 * Create a context from manual configuration.
 */
function ctools_facet_query_context_create_facet_query($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('facet_query');
  $context->plugin = 'facet_query';

  if ($empty) {
    return $context;
  }

  if ($conf) {
    if (!empty($_GET['f'])) {
      foreach ($_GET['f'] as $field) {
        list($key, $value) = explode(':', $field);
        if ($key == $data['key']) {
          $context->data = $value;
        }
      }
    }
  }
  return $context;
}

/**
 * Form builder; settings for the context.
 */
function ctools_facet_query_context_settings_form($form, &$form_state) {

  // build the options from enabled search api fields (facets?)
  $options = array();
  $indexes = search_api_index_load_multiple(FALSE);
  foreach ($indexes as $index) {
    if ($index->enabled && $index->server()->supportsFeature('search_api_facets')) {
      foreach ($index->options['fields'] as $field_name=>$field) {
        if (isset($field['entity_type'])) {
          $options[$field_name] = $field_name . t(' (entity)');
        }
        else {
          $options[$field_name] = $field_name;
        }
      }
    }
  }

  $form['key'] = array(
    '#title' => t('Facet query field name'),
    '#description' => t('Select the field to be retrieved from the query string. Fields marked with <i>(entity)</i> can be loaded using a relationship.'),
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => $options,
    '#default_value' => isset($form_state['conf']['key']) ? $form_state['conf']['key'] : NULL,
  );

  return $form;
}

/**
 * Submit handler; settings form for the context.
 */
function ctools_facet_query_context_settings_form_submit($form, &$form_state) {
  $form_state['conf']['key'] = $form_state['values']['key'];
}


/**
 * Provide a list of ways that this context can be converted to a string.
 */
function ctools_facet_query_context_convert_list($plugin) {
  return array(
    'raw' => t('Raw string'),
    'html_safe' => t('HTML-safe string'),
  );
}

/**
 * Convert a context into a string.
 */
function ctools_facet_query_context_convert($context, $type, $option = array()) {
  switch ($type) {
    case 'raw':
      return $context->data;
    case 'html_safe':
      return check_plain($context->data);
  }
}

