Ctools Facet Query Context

This module takes facet query strings (ex. f[0]=field_myfield:14) and creates a context
for panels to use. This module is based on the fantastic work done balintk on <a href="http://drupal.org/project/user/613760">CTools query string context</a>.

This module supports:
* pulling in a value from a facet query string
* loading an entity from a value in a facet query string via a relationship
